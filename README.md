# SESSION INSPECTOR HOSTNAME GEOCODER

A module helps with security as users can remove any sessions
from locations or devices that they don't recognise.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/session_inspector).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/session_inspector_hostname_geocoder).

## Requirements

This module requires the following modules:
- [Session Inspector](https://www.drupal.org/project/session_inspector)
- [Geocoder](https://www.drupal.org/project/geocoder)

## Installation

Install with composer (composer require `drupal/session_inspector_hostname_geocoder:^1.0`)

Enable the module.

## Configuration

Assuming you already have the session inspector module installed.

1. Enable the module at Administration > Extend.
2. Head over to the Geocoder provider configuration page at
admin/config/system/geocoder/geocoder-provider and ensure you have at least one
provider. You may need to install a provider first.
3. Head to the Session Inspector Configuration page at
`/admin/config/people/session_inspector` and change the hostname format to be
"Geocoder hostname format".

This plugin will attempt to get as much information as possible from the
geocoded location.

## Maintainers

- philipnorton42 - [philipnorton](https://www.drupal.org/u/philipnorton42)

Supporting organizations:

- [#! code (Hash Bang Code)](https://www.drupal.org/code-hash-bang-code)
