<?php

namespace Drupal\session_inspector_hostname_geocoder\Plugin\HostnameFormat;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\geocoder\Geocoder;
use Drupal\session_inspector\Plugin\HostnameFormatInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides full browser detector format.
 *
 * @HostnameFormat(
 *   id = "geocoer_hostname_format",
 *   name = @Translation("Geocoder hostname format")
 * )
 */
class HostnameGeocoderFormat extends PluginBase implements HostnameFormatInterface {

  /**
   * The Geocoder providers storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $geocoderProvider;

  /**
   * The Geocoder service.
   *
   * @var \Drupal\geocoder\Geocoder
   */
  protected $geocoder;

  /**
   * Constructs a new BookNavigationBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $geocoder_provider
   *   The Geocoder providers storage.
   * @param \Drupal\geocoder\Geocoder $geocoder
   *   The Geocoder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $geocoder_provider, Geocoder $geocoder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->geocoderProvider = $geocoder_provider;
    $this->geocoder = $geocoder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('geocoder_provider'),
      $container->get('geocoder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formatHostname(string $hostname):string {
    $providers = $this->geocoderProvider->loadMultiple();

    if (count($providers) == 0) {
      // No providers set up.
      return $hostname;
    }

    $addressCollection = NULL;

    try {
      // Attempt to geocode the hostname.
      $addressCollection = $this->geocoder->geocode($hostname, $providers);
    }
    catch (\Exception $e) {
    }
    catch (\TypeError $e) {
    }

    if ($addressCollection === NULL) {
      // Unable to geocode the address.
      return $hostname;
    }

    // Attempt to build the address string using the locality and country.
    $addressParts = array_filter([
      $addressCollection->get(0)->getLocality(),
      $addressCollection->get(0)->getCountry(),
    ]);

    if (count($addressParts) === 0) {
      return $this->t('Unknown');
    }

    return implode(', ', $addressParts);
  }

}
